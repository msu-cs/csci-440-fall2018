# Triggers & Views

We will finish up relational DBs by discussing triggers and views.  Triggers
allow us to perform SQL statements based on other operations that occur in the
DB.  Views allow us to create a layer of abstraction between a user of the DB
and the design we produce and can make queries simpler for the user.  Lets dive
in.

## Triggers

Triggers allow us to cause SQL statement to run on events, such as data updates.
Lets get an idea of what this means with an example.  (Note that we will make a
trigger that will cache computed data)

Lets use our old friend `2018-11-14-generate-date.py` to add some data to our
table.

    $ python 2018-11-14-generate-data.py`

Assume that we wanted to get the number of friends of each student.  We could
run the sql command:

    > select src, count(src)
        from (
            select ssn1 as src, ssn2 as target from Friends union
            select ssn2 as src, ssn1 as target from friends
        )
        group by src;

Now, lets assume that we wanted to manually handle a cache of these counts.  One
option is to add a table `FriendCountsCache` and ask the developer to update
both tables in a transaction or we could have the database do the heavy lifting
for us by having a trigger update the `FriendCountsCache` table.

First, lets create the friends counts table.  Add to `query.sql` the lines:

    drop table if exists FriendCountsCache;

    create table FriendCountsCache(
        ssn text,
        num_friends int
    )

Next lets add our trigger.  In general, triggers have the form:

    CREATE TRIGGER trigger_name [BEFORE|AFTER] [INSERT|DELETE|UPDATE]
    ON table_name
    BEGIN
     -- Trigger logic
    END;

So, we can get started adding our trigger with the code:

    create trigger incFriendCountsCache AFTER INSERT
    ON Friends
    BEGIN
        INSERT OR REPLACE INTO FriendCountsCache
        VALUES(new.ssn1, 1);
        INSERT OR REPLACE INTO FriendCountsCache
        VALUES(new.ssn2, 1);
    END;

    select * from FriendCountsCache;
    insert into Friends values('1', '2');
    insert into Friends values('1', '4');
    select * from FriendCountsCache;


I also added the following to drop the trigger while developing:

    drop trigger if exists incFriendCountsCache;

Note that in the code above we have the keyword `new`.  (TODO.... discuss `new`
and `old`, which is only useful for modify)

Now, if we run our script we get the output:

    ssn         num_friends
    ----------  -----------
    1           1
    2           1
    1           1
    4           1

... humm, so we now have two entries for ssn 1.  This can be fixed by marking
the `ssn` as a `PRIMARY KEY` in `FriendCountsCache`.

    create table FriendCountsCache(
        ssn text PRIMARY KEY,
        num_friends int
    );

Now lets run again and we see that the number of records is as expected.  So,
lets fix up our query to get the values that we want and add a few more insert
statements to make sure all is updated correctly to produce our final script:

    drop table if exists FriendCountsCache;

    create table FriendCountsCache(
        ssn text PRIMARY KEY,
        num_friends int
    );

    drop trigger if exists incFriendCountsCache;
    create trigger incFriendCountsCache AFTER INSERT
    ON Friends
    BEGIN
        INSERT OR REPLACE INTO FriendCountsCache
        VALUES(new.ssn1,
            (select distinct count(*)
            from (
                select ssn2 from Friends where ssn1 = new.ssn1 union
                select ssn1 from Friends where ssn2 = new.ssn1
            ))
        );
        INSERT OR REPLACE INTO FriendCountsCache
        VALUES(new.ssn2,
            (select distinct count(*)
            from (
                select ssn2 from Friends where ssn1 = new.ssn2 union
                select ssn1 from Friends where ssn2 = new.ssn2
            ))
        );
    END;

    select * from FriendCountsCache;
    insert into Friends values('1', '2');
    insert into Friends values('1', '4');
    insert into Friends values('3', '2');
    insert into Friends values('5', '2');
    select * from FriendCountsCache;

Now you try, this trigger will only update the counts when a friendship is
added.  Now, try the following:

1. Friendship is removed (hint: you will need to use `old`)
2. Friendship is modified (hint: you will need to use `old`)
3. Instead of recomputing update a count.

## Answer 1

Add a trigger for friendship removed

    CREATE TRIGGER decFriendCountsCache AFTER DELETE
    ON Friends
    BEGIN
        INSERT OR REPLACE INTO FriendCountsCache
        VALUES(old.ssn1,
            (select distinct count(*)
                from (
                    select ssn2 from Friends where ssn1 = old.ssn1 union
                    select ssn1 from Friends where ssn2 = old.ssn1
            ))
        );

        INSERT OR REPLACE INTO FriendCountsCache
        VALUES(old.ssn2,
            (select distinct count(*)
                from (
                    select ssn2 from Friends where ssn1 = old.ssn2 union
                    select ssn1 from Friends where ssn2 = old.ssn2
            ))
        );
    END;

## Answer 2

Add trigger for when friendship is modified

    CREATE TRIGGER updateFriendCountsCache AFTER UPDATE
    ON Friends
    BEGIN

        INSERT OR REPLACE INTO FriendCountsCache
        VALUES(old.ssn1,
            (select distinct count(*)
                from (
                    select ssn2 from Friends where ssn1 = old.ssn1 union
                    select ssn1 from Friends where ssn2 = old.ssn1
            ))
        );

        INSERT OR REPLACE INTO FriendCountsCache
        VALUES(old.ssn2,
            (select distinct count(*)
                from (
                    select ssn2 from Friends where ssn1 = old.ssn2 union
                    select ssn1 from Friends where ssn2 = old.ssn2
            ))
        );

        INSERT OR REPLACE INTO FriendCountsCache
        VALUES(new.ssn1,
            (select distinct count(*)
                from (
                    select ssn2 from Friends where ssn1 = new.ssn1 union
                    select ssn1 from Friends where ssn2 = new.ssn1
            ))
        );

        INSERT OR REPLACE INTO FriendCountsCache
        VALUES(new.ssn2,
            (select distinct count(*)
                from (
                    select ssn2 from Friends where ssn1 = new.ssn2 union
                    select ssn1 from Friends where ssn2 = new.ssn2
            ))
        );
    END;


