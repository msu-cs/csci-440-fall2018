// Q1: Create a collection named tvShows that has a name and array of characters
// Q2: Add a character to a show using the $push command


function insertCity(name, population, lastCensus, famousFor, mayorInfo){
  db.towns.insert({
    name, population, lastCensus: ISODate(lastCensus), famousFor, mayor: mayorInfo,
  });
}

// add some data
insertCity(
  "New York",
  22200000,
  "2016-07-01"
  ["statue of liberty", "food", "Derek Jeter"],
  { name: "Bill de Blasio", party: "D" }
);
insertCity(
  "Punxsutawney",
  6200,
  '2016-01-31',
  ["Punxsutawney Phil"],
  { name : "Richard Alexander" }
);
insertCity(
  "Portland",
  582000,
  '2016-09-20',
  ["beer", "food", "Portlandia"],
  { name : "Ted Wheeler", party : "D" }
);



// Q3: Select a town via a case-insensitive regular expression containing the word new.
// Q4: Find all cities whose names contain an e and are famous for food or beer.

